#boredQ

## How to?
boredQ works like a REST service.
### How to send?
Just send a json via POST to http://IP-OF-BOREDQ:6060/queue/NAMEOFQUEUE/append
Either with:
```
{"message": "Blub"}
```
Or send many:
```
{"messages": ["bla", "blub"]}
```

### How to receive?
GET http://IP-OF-BOREDQ:6060/queue/NAMEOFQUEUE
Or receive many:
POST to http://IP-OF-BOREDQ:6060/queue/NAMEOFQUEUE
```
{"messages": 5000}
```

### How to delete a queue?
DELETE http://IP-OF-BOREDQ:6060/queue/NAMEOFQUEUE

In case boredQ goes down, the messages are stored inside an sqliteDB and will be loaded into the queues at start time.

I haven't implemented anything else :)