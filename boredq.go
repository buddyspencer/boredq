package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	_ "github.com/mattn/go-sqlite3"
	log "github.com/sirupsen/logrus"
	"gitlab.com/buddyspencer/chameleon"
	"io/ioutil"
	"net/http"
	"os"
	"sync"
)

type Message struct {
	Message string
}

type MessageGetPost struct {
	Messages int
}

type MessageAppendPost struct {
	Message string
	Messages []string
}

var (
	ip = "0.0.0.0"
	port = 6060
	queues = map[string][]Message{}
	mutex sync.Mutex
	db = &sql.DB{}
)

func main() {
	customFormatter := new(log.TextFormatter)
	customFormatter.TimestampFormat = "2006-01-02 15:04:05"
	customFormatter.FullTimestamp = true
	customFormatter.ForceColors = true
	log.SetFormatter(customFormatter)
	log.SetOutput(os.Stdout)

	_, err := os.Getwd()
	hdb := fmt.Sprintf("queues.db")

	db, err = sql.Open("sqlite3", hdb)
	db.SetMaxOpenConns(1)
	db.Exec("PRAGMA journal_mode=WAL")

	log.Info("Adding data to queue")
	if _, err := os.Stat("queues.db"); !os.IsExist(err) {
		rows, err := db.Query(`select name from sqlite_master where type="table";`)

		if err != nil {
			log.Panic(err)
		}
		tables := []string{}
		for rows.Next() {
			var name string
			err = rows.Scan(&name)
			if err != nil {
				log.Panic(err)
			}
			if name != "sqlite_sequence" {
				queues[name] = []Message{}
				tables = append(tables, name)
			}
			err = rows.Err()
			if err != nil {
				log.Panic(err)
			}
		}
		rows.Close()
		for _, table := range tables {
			rows, err := db.Query(fmt.Sprintf("select message from %s", table))
			for rows.Next() {
				var message string
				err = rows.Scan(&message)
				if err != nil {
					panic(err)
				}
				queues[table] = append(queues[table], Message{message})
			}
			rows.Close()
		}
		log.Info("Added data to queue")
	}

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	r := mux.NewRouter()
	r.HandleFunc("/queue/{queue}", getQueue).Methods("GET")
	r.HandleFunc("/queue/{queue}", getQueuePost).Methods("POST")
	r.HandleFunc("/queue/{queue}/append", appendMessagePost).Methods("POST")
	r.HandleFunc("/queue/{queue}", deleteQueue).Methods("DELETE")
	http.Handle("/", r)

	log.Infof("Listening on http://%s:%d", ip, port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf("%s:%d", ip, port), r))
}

func deleteQueue(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	status := 200

	delete(queues, params["queue"])
	dropTable(params["queue"])

	w.WriteHeader(status)
	json.NewEncoder(w).Encode(Message{fmt.Sprintf("deleted %s", params["queue"])})
}

func appendMessagePost(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	body, err := ioutil.ReadAll(r.Body)
	params := mux.Vars(r)
	status := 200

	if err != nil {
		log.Error("Something went wrong: " + err.Error())
	}

	jsonBody := &MessageAppendPost{}

	err = json.Unmarshal(body, jsonBody)
	if err != nil {
		log.Error("Something went wrong: " + err.Error())
	}

	if _, ok := queues[params["queue"]]; !ok {
		queues[params["queue"]] = []Message{}
		statement, err := db.Prepare(fmt.Sprintf("CREATE TABLE IF NOT EXISTS %s (id INTEGER PRIMARY KEY AUTOINCREMENT, message VARCHAR)", params["queue"]))
		log.Infof("created queue %s", chameleon.Green(params["queue"]))
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		statement.Exec()
		statement.Close()
	}
	statement, _ := db.Prepare(fmt.Sprintf("INSERT INTO %s(message) VALUES (?)", params["queue"]))
	if jsonBody.Message != "" {
		mutex.Lock()
		queues[params["queue"]] = append(queues[params["queue"]], Message{jsonBody.Message})
		log.Infof("added message to %s", params["queue"])
		mutex.Unlock()
		statement.Exec(jsonBody.Message)
	} else {
		log.Errorf("empty message for %s", params["queue"])
	}

	if len(jsonBody.Messages) > 0 {
		messages := []Message{}
		for _, message := range jsonBody.Messages {
			messages = append(messages, Message{message})
			statement.Exec(message)
		}
		mutex.Lock()
		queues[params["queue"]] = append(queues[params["queue"]], messages...)
		mutex.Unlock()
		log.Infof("added message to %s", params["queue"])
	}

	w.WriteHeader(status)
	json.NewEncoder(w).Encode(Message{"added"})
}

func getQueue(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	m := Message{"queue is empty"}
	status := 500

	mutex.Lock()
	if len(queues[params["queue"]]) > 0 {
		m = queues[params["queue"]][0]
		status = 200
		queues[params["queue"]] = queues[params["queue"]][1:]
		deleteFromDB(params["queue"], 1)
	}
	mutex.Unlock()
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(m)
}

func getQueuePost(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		log.Error("Something went wrong: " + err.Error())
	}

	jsonBody := &MessageGetPost{}

	err = json.Unmarshal(body, jsonBody)
	if err != nil {
		log.Error("Something went wrong: " + err.Error())
	}

	params := mux.Vars(r)
	m := Message{"queue is empty"}
	status := 500

	mutex.Lock()
	x := []Message{}
	if len(queues[params["queue"]]) > 0 {
		status = 200
		if len(queues[params["queue"]]) > jsonBody.Messages {
			length := jsonBody.Messages
			x = queues[params["queue"]][:jsonBody.Messages]
			queues[params["queue"]] = queues[params["queue"]][jsonBody.Messages:]
			deleteFromDB(params["queue"], length)
		} else {
			length := len(queues[params["queue"]])
			x = queues[params["queue"]]
			queues[params["queue"]] = []Message{}
			deleteFromDB(params["queue"], length)
		}
	}
	mutex.Unlock()
	w.WriteHeader(status)
	if status == 200 {
		json.NewEncoder(w).Encode(x)
	} else {
		json.NewEncoder(w).Encode(m)
	}
}

func deleteFromDB(table string, row int) {
	statement, err := db.Prepare(fmt.Sprintf("Delete from %s where rowid IN (Select rowid from %s limit %d);", table, table, row))
	statement.Exec()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func dropTable(table string) {
	statement, err := db.Prepare(fmt.Sprintf("DROP TABLE IF EXISTS %s;", table))
	statement.Exec()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}